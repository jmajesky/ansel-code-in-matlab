import numpy as np
import sys, os
import matplotlib.pyplot as plt

def read_file(filename):
	datafile = open(filename)
	data = []
	for line in datafile.readlines():
		vals = line.split()

		data.append([vals[0], vals[1]])

	return np.array(data)

 def make_hist(data, binnumber):
 	hist , bin_edges = np.histogram(data, bins=binnumber)
	return hist, bin_edges

def show_hist(hist, bin_edges):
	print("Showing Hist")


def gauss_fit(histogram):
	print("Fitting function")

