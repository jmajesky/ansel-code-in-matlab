%% ansel_figure_prep: function description
function [outputs] = ansel_figure_prep(file_list, file_dir)
	
for idx=1:length(file_list)
file = file_list(idx).name;
degrees_after = strfind(file,'degrees');
degrees = str2num(file((degrees_after-3):(degrees_after-1)));


append_str = strcat(' ', int2str(degrees), ' degrees');
fi = openfig(strcat(file_dir,file));

angl = ~isempty(strfind(file, 'right_way'));
energy = ~isempty(strfind(file, 'endet'));
detector1 = ~isempty(strfind(file, 'det1'));


	if detector1
		detector = ' detector 1'
	else
		detector = ' detector 2'
	end

	if angl
		theta = ' Theta 2 '
	else
		theta = ' Theta 1 '
	end

	if energy
		units = ' KeV '

		x = 'Counts per KeV per second'

	else
		units = ' ADC '

		x = 'Counts per ADC'

	end

	tit = strcat(' Na22 ', units, ' Peak for ', theta, ' on ', detector , ' at ', append_str)

	y = strcat('Energy (', units, ')')

	title(tit)

	ylabel(x)

	xlabel(y)


	grid on

	ax = gca;


	ax.TickDir  = 'in'

	savefig(strcat(file_dir,file))
	saveas(fi, strcat('Pics/',file(1:(end -4))), 'jpg')
end


end