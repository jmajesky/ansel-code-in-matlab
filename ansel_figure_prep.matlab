%% ansel_figure_prep: function description
function [outputs] = ansel_figure_prep(file_list, file_dir)
	
for idx=1:length(file_list)
file = file_list(idx).name;
degrees_after = strfind(file,'degrees');
degrees = str2num(file((degrees_after-3):(degrees_after-1)));


append_str = strcat(int2str(degrees), ' degrees');
openfig(strcat(file_dir,file));

angl = ~isempty(strfind(file, 'right_way'));
energy = ~isempty(strfind(file, 'endet'));
detector1 = ~isempty(strfind(file, 'det1'));


	if detector1
		detector = ' detector 1'
	else
		detector = ' detector 2'
	end

	if angl
		theta = 'Theta 1 '
	else
		theta = 'Theta 2 '

	if energy
		units = 'KeV '

		x = 'Counts per KeV'

	else
		units = 'ADC '

		x = 'Counts per second per KeV'

	end

	tit = strcat('Na22 ', units, 'Peak for ', theta, ' on', detector)

	y = strcat('Energy (', units, ')')

	title(tit)

	ylabel(x)

	xlabel(y)


	grid on

	ax = gca;


	ax.TickDir  = 'in'

	savefig(strcat(file_dir,file))
end


end