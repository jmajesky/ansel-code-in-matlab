function [Cut_Data] = ansel_preprocess(Data, Lower_Cut, Upper_Cut)

Cut_Data = Data(Data>Lower_Cut);
Cut_Data = Cut_Data(Cut_Data<Upper_Cut);

end

