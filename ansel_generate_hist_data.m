function [gauss_fit, gauss_gof] = ansel_generate_hist_data(data_vector, binnumber, name, peaknums, cutnum)

%Generate and save figure

hist = histogram(data_vector, binnumber);


if cutnum ==0 
	cut = 1;
else
	cut = binnumber/4;
end
num = int2str(peaknums);
[gauss_fit, gauss_gof] = fit(hist.BinEdges((1 + cut):end).', hist.Values(cut:end).', strcat('gauss', num), 'Upper', 10000000000);
%gauss_mean = gauss_fit.b1;
%gauss_variance = gauss_fit.c1;

%histogram_data = [gauss_mean, gauss_variance]
%x= linspace(min(data_vector(:)), max(data_vector(:)));
%y = gauss_fit.a1 * exp(((x - gauss_fit.b1)/ gauss_fit.c1).^2);
hold on
plot(gauss_fit, hist.BinEdges((1 + cut):end), hist.Values(cut:end));
savefig(name);
hold off
end
