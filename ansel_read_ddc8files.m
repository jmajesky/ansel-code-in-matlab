function [Data] = ansel_read_ddc8files(data_file, channelnum, Lower_Cut, Upper_Cut)

include = '%d ';
declude = ' %*d';
formatspec = strcat(repmat(include , 1, channelnum) ,  strcat(' ', repmat(declude , 1, (9 - channelnum))));

file = fopen(data_file);

Data = fscanf(file, formatspec, [channelnum Inf]);
%size(Data)
%Data = preprocess(Data, Lower_Cut, Upper_Cut);
fclose(file);
end

function [Cut_Data] = preprocess(Data, Lower_Cut, Upper_Cut)

Cut_Data = Data(Data>Lower_Cut);
Cut_Data = Cut_Data(Cut_Data<Upper_Cut);
size(Cut_Data);
end

