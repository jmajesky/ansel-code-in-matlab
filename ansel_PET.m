function [theta1_degreees_v_counts, theta2_degreees_v_counts, theta1_data, theta2_data, det1_regress, det2_regress, det1_hist, det2_hist] = ansel_PET(in)


detector1file = 'Paper/20160223Na22Det1FullSpectrum.txt';
detector2file = 'Paper/20160223Na22Det2FullSpectrum.txt';
detector1figurefile = 'Na22Det1ADC';
detector2figurefile = 'Na22Det2ADC';

runtimes_1 = [219.103, 120.3, 124.6, 116.05, 173.2, 120.3, 223.7, 173.8];

runtimes_2 = [162.3, 373.1, 186.9, 127.5, 110.9, 132.1, 151.3];


[det1_regress, det1_hist] = energy_calc([511, 1275], detector1file, detector1figurefile, -32000, 15000, 4);

[det2_regress, det2_hist] = energy_calc([511, 1275], detector2file, detector2figurefile, -32000, 5000, 0);



det1_adc = ansel_read_ddc8files(detector1file, 1);

det1_adc = ansel_preprocess(det1_adc, -32000, 15000);

%Compute Energy from ADC
[det1_en, det1_variance]  = energy(det1_adc, det1_regress);
ansel_generate_hist_data(det1_en, 600, 'energydet1', 2,0)

det2_adc = ansel_read_ddc8files(detector2file, 1);
det2_adc = ansel_preprocess(det2_adc, -32000, 5000)
%Compute Energy from ADC
det2_en  = energy(det2_adc, det2_regress);
ansel_generate_hist_data(det2_en, 600, 'energydet2',2, 0)
figure

%Batch convert all Data Files for unknown source
hidden_file_dir = '/home/joseph/ANSEL_LAB_3/Data/Hidden/'; 
hidden_source_files = dir('/home/joseph/ANSEL_LAB_3/Data/Hidden/*.txt');
hidden_file_dir2 = '/home/joseph/ANSEL_LAB_3/Data/Hidden2/'; 
hidden_source_files2 = dir('/home/joseph/ANSEL_LAB_3/Data/Hidden2/*.txt');




[theta1_degreees_v_counts, theta1_data] = compute_degrees_per_count(hidden_source_files, hidden_file_dir, det1_regress, det2_regress, runtimes_1);




[theta2_degreees_v_counts, theta2_data] = compute_degrees_per_count(hidden_source_files2, hidden_file_dir2, det1_regress, det2_regress, runtimes_2);
%Take Gaussian distribution of Energies for each detector





%Calculate Optimal Theta
figure
theta1_fit = fit(theta1_degreees_v_counts(1,:)', theta1_degreees_v_counts(2,:)', 'gauss1');
theta2_fit = fit(theta2_degreees_v_counts(1,:)', theta2_degreees_v_counts(2,:)', 'gauss1');

errorbar(theta1_degreees_v_counts(1,:), theta1_degreees_v_counts(2,:), theta1_degreees_v_counts(3,:)/2);
hold on
plot(theta1_fit, theta1_degreees_v_counts(1,:), theta1_degreees_v_counts(2,:));
savefig('Theta1vsCounts')
figure
errorbar(theta2_degreees_v_counts(1,:), theta2_degreees_v_counts(2,:), theta2_degreees_v_counts(3,:)/2);
hold on
plot(theta2_fit, theta2_degreees_v_counts(1,:), theta2_degreees_v_counts(2,:));
savefig('Theta2vsCounts')


%Convert to XY space

	opt_theta1 = theta1_fit.b1
	opt_theta2 = theta2_fit.b1
	opt_theta1_error = theta1_fit.c1
	opt_theta2_error = theta2_fit.c1

	theta_tot_error = sqrt(opt_theta2_error^2 + opt_theta1_error^2)

	line_1= 6.35 * sind(180 - opt_theta1) / (cosd(180 - opt_theta1) +  6.35) 
	line_1_offset = 6.35 * sind((180 - opt_theta1)/2) / cosd((180 - opt_theta1)/2)

	line_2 = (sind(90 - opt_theta2) - 1)/ cosd(90 - opt_theta2)

	line_2_offset = 6.35

	%line_1 = - sin(.5 * opt_theta1) * sin((180 -opt_theta1) / 2) 
	%line_1_offset = sin(.5 * opt_theta1) * sin((180 -opt_theta1) / 2) 

	%line_2 = -sin(.5 * (opt_theta2)) * sin((180 -(opt_theta2 - 90))) 
	%line_2_offset = 6.35 %- sin(.5 * (opt_theta2 -90)) * sin((180 -(opt_theta2 - 90))) 

	source_x = (-line_2_offset + line_1_offset )/ (line_1 - line_2)
	source_y = line_1 * source_x + line_1_offset

	
	%Compute 



end


function [regression_coefficients, det_hist] = energy_calc(energies,detectorfilename, figurefilename, lc1, uc1, cutnum)
det_data = ansel_read_ddc8files(detectorfilename, 1);
det_data = ansel_preprocess(det_data, lc1, uc1);
%det2_data = ansel_read_ddc8files(detector2file, 1, lc2, uc2);

det_hist = ansel_generate_hist_data(det_data, 1000, figurefilename, 2, cutnum);

A = [det_hist.b1 det_hist.c1;  det_hist.b2, det_hist.c2]
adc_weights =  sort([det_hist.b1 det_hist.c1;  det_hist.b2, det_hist.c2]);
adc = adc_weights(:,1);
weights = adc_weights( :, 2);
energies;

en_data = ansel_lin_fit(adc, weights, energies');

regression_variance = en_data.gof.sse;

%WRONG VARIANCE!!!
total_variance = sqrt(1/det_hist.c1 + 1/det_hist.c2 + regression_variance);

regression_coefficients = [en_data.fit_data.p1, en_data.fit_data.p2, total_variance];

end


function [energy_data, variance] = energy(data, regressors)
energy_data = regressors(1) .* data + regressors(2);
%energy_data = polyval(regressors(1:2), data);
variance = regressors(3);
end



function [cumulative_counts, integral_errors] = count_events(hist_object)
syms x clear;

cumulative_counts  = int(hist_object.a1 * exp(((x- hist_object.b1)/hist_object.c1)^2), hist_object.b1 - 1* hist_object.c1 ,1* hist_object.c1 + hist_object.b1)

if cumulative_counts ==0
	return

end
conf_bounds = confint(hist_object)

integral_errors = conf_bounds(2,:) - conf_bounds(1,:);

integral_errors = sqrt(sum(integral_errors(:).^2))

end


function [files_list] = get_files(directory)

hidden_source_files = dir(strcat(directory, '.txt'));

end


function [degrees_v_counts, data] = compute_degrees_per_count(hidden_source_files, hidden_file_dir, det1_regress, det2_regress, runtimes)
degrees_v_counts = zeros(3, size(hidden_source_files,2));

%Data table. Has the form 
%Theta number
%Degrees
%Gauss Fit a1
%Gauss fit b1
%Gauss Fit c1
%Gauss fit a2
%Gauss fit b2
%Gauss fit c2
%Standard error det1
%Standard error det2
%Counts det1

data = zeros(1, 20, 20);

for idx = 1:numel(hidden_source_files)
	%String processing
	source_file = hidden_source_files(idx)
	degrees_after = strfind(source_file.name,'degrees');
	degrees = str2num(source_file.name((degrees_after-3):(degrees_after-1)));

	%Get data from file
	adc_data = ansel_read_ddc8files(strcat(hidden_file_dir, source_file.name), 2);

	%FIXING IT DONT
	adc_data(1, :) = adc_data(1,:) - 280;
	adc_data(2,:) = adc_data(2,:) - 10750;

	det1_adc = ansel_preprocess(adc_data(1,:), -32000, -10000);
	det2_adc = ansel_preprocess(adc_data(2,:), -32000, -20000);

	%Generate ADC Histogram
	[det1_hist, det1gof]  = ansel_generate_hist_data(det1_adc, 640, strcat('Figures/', strcat(source_file.name(1:(end-4)), 'det1figure')),1, 1);
	[det2_hist, det2gof] = ansel_generate_hist_data(det2_adc, 640, strcat('Figures/',strcat(source_file.name(1:(end-4)),'det2figure')),1, 1);

	%Covnert to eV
	[det1ens, det1_var] = energy(adc_data(1,:), det1_regress);
	[det2ens, det2vars ]= energy(adc_data(2,:), det2_regress);


	
	scatter(det1ens, det2ens);
	savefig(strcat('Figures/',strcat(source_file.name(1:(end-4)),'scatterfigure')))

	%Cut around 511 peak
	det1_en_data = ansel_preprocess(det1ens,440, 580 );
	det2_en_data = ansel_preprocess(det2ens, 440, 580);

	%ev Histogram
	[det1_enhist det1engof] =  ansel_generate_hist_data(det1_en_data, 100, strcat('Figures/',strcat(source_file.name(1:(end-4)), 'endet1figure')), 1, 1);
	[det2_enhist , det2engof]=  ansel_generate_hist_data(det2_en_data, 100, strcat('Figures/',strcat(source_file.name(1:(end-4)), 'endet2figure')),1, 1);


	%Get counts of data by integrating
	[det1_counts, det1_int_error]= count_events(det1_enhist);
	[det2_counts, det2_int_error] = count_events(det2_enhist);


	%Record information
	degrees_v_counts(1, idx) = degrees;

	degrees_v_counts(2, idx) = (det1_counts + det2_counts)/2;
	degrees_v_counts(2, idx) = degrees_v_counts(2, idx) / runtimes(idx);

	degrees_v_counts(3, idx) =( det1_int_error + det2_int_error )/ (2 * runtimes(idx));

	%Record data 
	data(1, idx, 1) = 1;
	data(1, idx, 2) = degrees;
	data(1, idx, 3) = det1_enhist.a1;
	data(1, idx, 4) = det1_enhist.b1;
	data(1, idx, 5) = det1_enhist.c1;
	data(1, idx, 6) = det2_enhist.a1;
	data(1, idx, 7) = det2_enhist.b1;
	data(1, idx, 8) = det2_enhist.c1;
	data(1, idx, 9) = sqrt(det1engof.rmse^2 + det1_var^2 );
	data(1, idx, 10) = sqrt(det2engof.rmse^2 + det2vars^2);
	data(1, idx, 11) = degrees_v_counts(2,idx);
	data(1, idx, 12) = degrees_v_counts(3,idx);
end
end
