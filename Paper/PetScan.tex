% ****** Start of file apssamp.tex ******
%
%   This file is part of the APS files in the REVTeX 4.1 distribution.
%   Version 4.1r of REVTeX, August 2010
%
%   Copyright (c) 2009, 2010 The American Physical Society.
%
%   See the REVTeX 4 README file for restrictions and more information.
%
% TeX'ing this file requires that you have AMS-LaTeX 2.0 installed
% as well as the rest of the prerequisites for REVTeX 4.1
%
% See the REVTeX 4 README file
% It also requires running BibTeX. The commands are as follows:
%
%  1)  latex apssamp.tex
%  2)  bibtex apssamp
%  3)  latex apssamp.tex
%  4)  latex apssamp.tex
%
\documentclass[a4paper]{article}
\usepackage{titling}
\usepackage{graphicx}% Include figure files
\usepackage{tabu}
\usepackage{csvsimple}
\usepackage{gensymb}
\usepackage{amsmath}
\newcounter{equationset}
\newcommand{\equationset}[1]{
  \refstepcounter{equationset}
  \noindent\makebox[\linewidth]{Equation set~\theequationset: #1}}
\usepackage{bm}% bold math
%\usepackage{hyperref}% add hypertext capabilities
%\usepackage[mathlines]{lineno}% Enable numbering of text and display math
%\linenumbers % Commence numb lines

\begin{document}


\title{PET Scans with Sodium Iodide Scintillation Detectors}% Force line breaks with \\

\author{Joseph Majesky \& Rachel Gelfand}


\date{March 19, 2016;\\ Experiment: February 4-23, 2016  }% It is always \today, today,
             %  but any date may be explicitly specified
\begin{titlingpage}
\maketitle
\begin{abstract}
PET scans are used in medical imaging to generate images of a region of concern in a patient. In this experiment, a simple form of PET imaging is performed using two sodium iodide scintillation detectors. The location of a hidden Na22 source on the lab bench was found by performing two perpendicular angular sweeps across the bench and finding the angular separation between both detectors that gave the highest 511 KeV-emission count. The predictions made from the analysis of the scan indicated the location of the source within 1 cm of it's actual location, demonstrating the capability of the PET scanning technique to locate radiation emitting objects with relatively high precision on a macroscopic scale.
\end{abstract}
\end{titlingpage}

%\tableofcontents

\section{Introduction}
PET Scans are used in medical imaging to show the structure of organs and tissues in the body after a radioactive tracer is placed in the patient. Through the use of positron-emitting nucleides as tracers, a PET scan is capable of non-invasively exposing abnormalities in the organs and tissues. By scanning over the entire region of interest, a map of count rates of $\gamma$-emissions can be found at every location, creating an image of the area scanned.  
\section{Theory}
\subsection{Positron Annihilation}
 $\beta^{+}$ decay occurs when a Proton in a nucleide decays into a neutron, a positron, and an electronic neutrino. As the positron leaves the nucleus, it will come in contact with the electron cloud of the atom, where it will encounter electrons ($\beta^{-}$) from the electron cloud. Many positrons will thus annihilate with electrons, creating two $\gamma$-rays traveling in opposite directions due to conservation of linear momentum. Because of energy conservation, these $\gamma$'s will be exactly 511 KeV, equivalent to a single electron's rest mass. 

\subsection{Coincidence Events}
PET scans make use of multiple detectors to gather positional information about an area scanned. Due to the presence of background emissions as well as source emissions that are not of interest to the experimenter, a simultaneous event as seen by the detector can be caused by two unrelated events instead of two $\gamma$-emissions from a singular decay event. Since the events are independent, the joint probability of these events is given by $ P_{1,2} = P_1 * P_2$. The joint probability signifies the likelihood of two independent events occurring instantenously. In Na22, this could be a simultaneous emission of two 511 KeV $\gamma$'s and a single 1275 KeV $\gamma$-ray. \\
%FINISH FIGURE
\begin{figure}

\caption{\newline $P_j$ represents the probability of event j occurring per unit time.\newline $N_i$ represents the fraction of events that will be emitted at the appropriate angle from the source as to be incident on the detector surface (This is computed using the solid angle formula from [2]) \newline Equation 1 shows the probability event j will be detected by detector $i$.\newline Equation 2 shows the probability of detector i picking up event j in time k.\newline Assuming independent events, equation 3 gives the likelihood of two simultaneosu events being picked up in the same timeframe.}

\begin{equation}
N_ij = N_i * P_j
\end{equation}

\begin{equation}
N_{ijk} = N_i * P_j * T_k
\end{equation}
\begin{equation}
N_{i_1 i_2 j_1 j_2 k_1} = N_{i_1 j_1 k_1} * N_{i_2 j_2 k_1}
\end{equation}

\end{figure}

\subsection{PET Scan Source Location}

To find the location of a point source with a PET scan, the conservation of linear momentum in $\gamma$-emissions from positron emitting nucleides is exploited. When using two detectors, one can be held at a fixed location while the other is swept across a series of angles from the original detector. This essentially seeks out a line on which the source lies. The line on the source will be the chord between the two detectors at the angle where the most non-coincidence events are recorded. To reduce this to a point, another location is chosen for the fixed detector, and a second sweep is performed by the other detector. Another maximal angle will be found, which will give another chord between the detectors, and thus another line. The intersection point of these two lines will be the location of the source. See the Data Analysis section for the equations relating to this specific experimental setup.
\section{Experimental Setup and Procedure}
\subsection{Detectors, Equipment, and Setup}
Two sodium iodide detectors were used in this experiment. The detectors each were approximately 3 inches in diameter. They were placed on a circular lab bench with angular notches every 5 degrees to place the detectors in. On the center of the bench was a 12.7 cm circular platform where the source could be placed. Both detectors were biased to -1900 volts. A signal cable went from each detector to an amplifier where the signal amplitude was increased. Since the rise time of both detectors was on the order of nanoseconds, a .5 $\mu$-second shaping time was used in both amplifiers. Each amplifier outputted a uni-polar and bipolar signal. The uni-polar signals were sent directly to the DDC-8 for data collection. The bi-polar outputs were used for triggering, and were sent to a TSCA to set lower and upper thresholds on the triggers.  The lower threshold was raised to cut off everything lower than the 511 KeV Na22 emission, and the upper threshold was set low enough to not record the 1275 KeV Na22 emission. From the TSCA, the negative output signals were sent to a gate generator to control their width. A width of approximately 500 nanoseconds was chosen for both bi-polar signals. After this, the signals were sent to a logic unit, where a coincidence level of 2 was set. The output of the logic unit would only fire if at any point in time both signal inputs were active. The output of the logic unit was sent to the DDC-8 to use as the trigger. The use of the logic unit minimized possible coincidence events from being recorded. The only way an invalid event could be detected would be to have two unrelated events occur within 1000 nanoseconds of each other. Further coincidence event reduction was performed as part of Data Analysis to catch any remaining coincidence events. Figure 2 below shows the experiment schematic and the lab bench where the detectors were located.

\begin{figure}
\caption{The experimental setup schematic. The blue angle represents the angular separation between the detectors, referenced as $\Theta$. The Procedure relies on varying this angle to find the location of a source. From The detector, the signal is sent to amplifiers, which output uni-polar and bi-polar pulses with higher amplitudes than the  input signal. The uni-polar outputs are sent to the DDC-8 for digitization and represent the energy of an event. The bi-polar outputs are used as triggers for the DDC, so they are passed through a TSCA and Delay generator to set thresholds and widths of the signals. Finally, a logical unit is used to perform a logical AND of the signals, only allowing an output signal if both triggers are active at the same time. This is the trigger used to tell the DDC-8 to record an event.}
\includegraphics[width = 1\textwidth]{Diagram1.png}
\end{figure}
\subsection{Procedure}
To perform the PET scan, one detector was placed 6.35 cm away from the center of the platform where the source was resting. Then the other detector was placed $180 \degree$ from the original detector. For notational convenience, the fixed detector will be called Detector 2 and the non-fixed detector will be called Detector 1. The angle of separation between the two detectors will be referred to as $\Theta$. Data was recorded for varying degrees of $\Theta$ with an unhidden source placed exactly at the center of the platform. After data collection, Detector 1 was moved to another angular notch to change the value of $\Theta$ before the next data set was recorded. This process of changing angles and recording data will be referred to as an angular sweep. Working with an unhidden source served to confirm the behavior of the experimental setup before performing PET, and identify any necessary changes to the setup, of which none were found. The accuracy of the PET scan location prediction on this unhidden source can also be used as a second test of the methodology of this experiment. After the angular sweeps were done, the Na22 source was placed at an unknown location on the platform and the platform was covered up with an opaque cover. \\
After the source was covered, Detector 1 was moved back to it's initial position, and another angular sweep was performed. This angle was named $\Theta_1$. As stated above, one angular sweep can identify a line that the source lies on. To find the the actual point location of the source, Detector 2 was moved $90\degree$ clockwise from it's location for the $\Theta_1$ data collection, a second angular sweep was performed with Detector 1 for a similar range of angle magnitudes. This sweep was denoted as $\Theta_2$. Using the positions of Detector 2 as the X and Y axes of a Cartesian coordinate frame whose origin was at the center of the platform, geometric analysis was done to find the XY location of the source. After collecting data for $\Theta_1$ ranging from $130\degree$ to $170\degree$ and for $\Theta_2$ ranging from $110 \degree$ to $190 \degree$, the hidden cover of the source was removed and it's actual location measure. Using the aforementioned coordinate system as a reference, the XY location of the source was found to be at ($-1.1, 1.73$) centimeters. This number would be used as a comparison for the prediction made from the PET Scan. This process is illustrated in Figure 3. 

\begin{figure}
\caption{An illustration of the angular sweeping procedure. Detector 2 is held at a constant location while Detector 1's location relative to Detector 2 is changed. The Cartesian plane used to define the location of the hidden source is super imposed on the platform in this example.}
\includegraphics[width = 1\textwidth]{Diagram3.jpg}
\end{figure}

\subsection{Calibration}
To calibrate the detectors for ADC to KeV conversion, the thresholds set by the TSCA were removed allowing for the whole emission spectra to be recorded. The inputs to the logic unit were taken out, and each signal was used as a trigger in the DDC-8 (replacing the logic unit output) to record data from its corresponding detector in two separate data collections. Using the procedure outlined in [2] and [3], the two Na22 $\gamma$-peaks were used to create a linear conversion from the ADC units to KeV. 

\section{ Data Analysis}
All Data Analysis was done using MATLAB. The process was to first find the linear fit to convert ADC units to KeV, using the same process as described in [2] and [3].  Once this was done, a histogram of each data set was created and a Gaussian fit was performed on it. The mean, variance, and their respective errors were recorded. Coincidence events were then found and removed from each set of data. Integration of the Gaussian fit was performed with a lower bound of the fit's mean minus 2 standard deviations, and an upper bound of the fit's mean plus 2 standard deviations. This integral was used to represent the total number of events that occurred in a given detector for a given angle of separation. Each dataset was divided by the data collection runtime for that data set to get an Event count rate for each detector. This was done for all $\Theta_1$ and $\Theta_2$ data sets. The $\Theta_1$ angles and $\Theta_2$ angles then each were plotted against the total number of counts for that angle. A Gaussian fit was done on these plots, and the maximum value of the fit was assumed to be the optimal separation angle, $\Theta_{max}$ for a given angular sweep. Using geometric principles discussed in the section below, $\Theta_{1max}$ and $\Theta_{2max}$ were then used to find a point on the XY Cartesian plane with the origin at the center of the circular platform. The values found for these coordinates were $ -.924 \pm 1.90, 1.31 \pm 1.87 $ centimeters, a location .21 centimeters away from the measured location. 

\subsection{Removing Coincidences}
Using the solid angle and intrinsic efficiency calculations (explained in [2]) of how many events should be expected per second on either detector, it was found that 556.5 events should be detected by either detector per second. Given a gate width of 500 nanoseconds, there should be $\approx .00027$ events per 500 nanoseconds on either detector. This means for a given 500 nanosecond period, there should be a $.027\%$ of having a event be picked by a detector. Using the basic independence relations explained in Figure 1, this means there will only be a $\approx .003\%$ chance of having two independent events occur within the timeframe of 1000 nanoseconds required to trigger a recording of the event. Because of this calculation, it is assumed that the number of accidental coincidence events will be minimal to begin with. 
To remove any remaining coincidence events, the distances of each recorded event for both detectors from the mean of the data for the detectors was measured (ie since an event is associated with a signal from detector 1 and a signal from detector 2, the detector 1 signal is compared to the distribution from detector 1 for that given trial and likewise for detector 2). If a given event was within two standard deviation's of the corresponding fit for both detectors, it was kept. Likewise, if the event was outside two standard deviations of the fit for both detectors, the point was kept, since although it was an outlier, it was not going to affect the calculation of total counts later on. If a given event however, was only within 2 standard deviation's for one detector, it was removed from the dataset, since it is much more likely to be a coincidence event then a twin 511 KeV detection. This process can be seen on the scatter plots in Figure 4.  

\begin{figure}
\caption{An illustration of the coincidence removal process. Note the change in axes ranges after the coincidence removal. This particular example is from $\Theta_1$ for 170 degree separation. }
\includegraphics[width = .5\textwidth]{Pics/beforescatter.png}
\includegraphics[width = .5\textwidth]{Pics/afterscatter.png}
\end{figure}
\subsection{Counting Events}
To count the events detected for a given angle, an integral over the fits of each detector was done with bounds set two standard deviations apart from the means. To correct for the removed coincidence events, the number of events removed from within the bounds of integration fro a given detector was subtracted from the integral. This value was then averaged to give an estimate of the number of positron-decays detected by the detector at a given angle. The decays per second rate was then found by dividing the counts for all angles with respect to the runtimes of each corresponding test, which would allow for effective comparison of the number of decays seen by the detectors for a given angle of separation. This relationship between angle and count was plotted for both $\Theta_1$ and $\Theta_2$, and the maximum of a Gaussian fit of each plot was taken as the optimal angles $\Theta_{1max}$ and $\Theta_{2max}$. For this experiment, these values were found to be $\Theta_{1max} = 149.6\degree \pm 17.5\degree $ and $\Theta_{2max} = 155.2\degree \pm 17.1\degree$. The process can be seen in the equations below. 


\begin{figure}
\caption{$\Theta_1$ vs Events per second.}
\includegraphics[width = 1\textwidth]{Pics/theta1.jpg}
\end{figure}

\begin{figure}
\caption{$\Theta_2$ vs Events per second}
\includegraphics[width = 1\textwidth]{Pics/theta2.png}
\end{figure}


\subsection{Geometric Analysis}
$\Theta_{1max}$ and $\Theta_{2max}$ were converted to XY coordinates via the following equations, whose derivation is motivated by the illustration in Figure 3 of the experimental setup. \\ \\
1. For any given experimental setup, each detector is placed a distance R away from the origin point of our Cartesian plane (the center of the lab bench platform). The angle between the two detectors is given as $\Theta_{1max}$. \\
2. The line along which detector 2 initially lies is defined as the X-axis. This means detector 2's location is defined as (R, 0) in XY coordinates. \\
3. The coordinates of detector 1 are thus given by:\\
\begin{equation}
Y_{\Theta_{1max} } = R * \sin{(180 - \Theta_{1max})}
\end{equation}

\begin{equation}
X_{\Theta_{1max}} = R *\cos{ (180 - \Theta_{1max} )}
\end{equation}
4. These two points are used to define the slope of the line relating to $\Theta_{1max}$ The offset is given by: \\
\begin{equation}
b = R * \frac{\sin{\frac{(180 - \Theta_{1max})}{2} }} { \cos{\frac{(180 - \Theta_{1max})}{2} }}
\end{equation}
5. This effectively gives us our first line. For the second line, we define the rotated detector 2's new location as on the positive y axis, so it's coordinates are now (0, R). The XY coordinates of the second detector can then be computed by: \\
\begin{equation}
Y_{\Theta_{2max}} = R * \sin{(90 - \Theta_{2max})}
\end{equation}
\begin{equation}
X_{\Theta_{2max}} = R * \cos{(90 - \Theta_{2max})}
\end{equation}
6. The offset for line 2 is just R, so this effectively gives us our second line. 
7. From here, solve  for the intersection point of the two lines to find the XY coordinates of the source from $\Theta_{1max}$  $\Theta_{2max}$.\\
\\
Using this process, $\Theta_{1max} = 149.6\degree \pm 17.5\degree $ and $\Theta_{2max} = 155.2\degree \pm 17.1\degree$ were found to relate to the (X,Y) point
($ -0.925 \pm 1.90 $ , $ 1.311 \pm 1.87$) centimeters. 



\section{Discussion}
The predictions from the PET scan were .21 centimeters away from the source's measured location. Given that the data analysis treated the detectors as points instead of being finitely sized as well as the fact that the source itself was on the order of a centimeter in size rather than a point source, the predictions for the PET scan seem to be reasonable. The scatter plots in the figure above indicate that the accidental events were minimized compared to the total number of events collected even before the outlier removal, so it is safe to conclude that each data set represented only positron emissions from the Na22 source. This is reinforced by the expected calculation of .00278 events occurring per 500 nanoseconds in either detector, implying a very low liklihood of recording accidental events with the setup. 
\section{Conclusion}
The goal of this experiment was to study how $\beta^+$ decay behavior (specifically the annilihation of emittted positrons in the electron cloud) is exploited by PET scans to image an obscured area. PET was performed using two sodium iodide detectors to locate a Na22 source on a lab bench, which was found to be within .21 centimeters of it's known location. To handle coincidence events, a logic unit was used during data collection to only accept simultaneous events, and outlier removal was performed during data analysis to catch any remaining events. Given this level of accuracy, the capability of PET scans is believed to have been successfully demonstrated. 

\section{Citations}
1. Knoll, Glenn F. Radiation Detection and Measurement 3rd Edition. Copyright 2000. \\
2. Gelfand, Rachel and Majesky, Joseph. Gamma Spectroscopy with Sodium Iodide Scintillation Detectors. February 11, 2016. \\
2. Gelfand, Rachel and Majesky, Joseph. Gamma Spectroscopy with Germanium Detectors. March 3, 2016. 
\clearpage




\section{ Appendix}
\subsection{Data Tables}
The following data tables show results from each data set for $\Theta_1$ and $\Theta_2$. Each row contains the mean, variance, height, and error in the gaussian fits for detector 1 and detector 2, as well as the count rate and the error in the count rate for the test. The units on the means and variances are KeV. The Count Rate is in events detected per second. \\ 
\\
\large 
 $\Theta_1$ Data\\
\\
\tiny
\csvautotabular{Data.csv}
\newline
\newline
\newline
\large
$\Theta_2$ Data\\ 
\\
\tiny
\csvautotabular{Data2.csv}

\large
\clearpage
\subsection{Linear Conversion}
For detector 1, a linear transformation of $KeV = .03336 * ADC + 952.5$ was found. For detector 2, the transformation was $KeV = .04568 * ADC + 1863$. 
\begin{figure}
\caption{The Linear Conversion for both Detectors. }
\includegraphics[width = .5\textwidth]{Pics/Na22Det2ADC.png}
\includegraphics[width = .5\textwidth]{Pics/Na22Det1ADC.png}
\includegraphics[width = .5\textwidth]{Pics/energydet2.png}
\includegraphics[width = .5\textwidth]{Pics/energydet1.png}
\end{figure}

\clearpage
\subsection{Theta 1 Figures}

These are the figures for the $ \Theta_1$ angle measurement. $\Theta_1$ had Detector 2 placed at a fixed location on the lab bench. From the lab bench markings, this position was noted as being $130 \degree$. 

\begin{figure}
\caption{Event's captured by 110 degree separation between detector 2 and detector 1. Detector 2's absolute location was 130 degrees. Detector 1's absolute location was 240 degrees.}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_110degrees_onback_HIDDENdet1figure.jpg}}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_110degrees_onback_HIDDENendet1figure.jpg}}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_110degrees_onback_HIDDENdet2figure.jpg}}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_110degrees_onback_HIDDENendet2figure.jpg}}
\end{figure}

\begin{figure}
\caption{Event's captured by 120 degree separation. Detector 2's absolute location was 130 degrees. Detector 1's absolute location was 250 degrees.}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_120degrees_onback_HIDDENdet1figure.jpg}}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_120degrees_onback_HIDDENendet1figure.jpg}}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_120degrees_onback_HIDDENdet2figure.jpg}}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_120degrees_onback_HIDDENendet2figure.jpg}}
\end{figure}

\begin{figure}
\caption{Event's captured by 130 degree separation. Detector 2's absolute location was 130 degrees. Detector 1's absolute location was 260 degrees.}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_130degrees_onback_HIDDENdet1figure.jpg}}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_130degrees_onback_HIDDENendet1figure.jpg}}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_130degrees_onback_HIDDENdet2figure.jpg}}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_130degrees_onback_HIDDENendet2figure.jpg}}
\end{figure}


\begin{figure}
\caption{Event's captured by 150 degree separation. Detector 2's absolute location was 130 degrees. Detector 1's absolute location was 280 degrees.}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_150degrees_onback_HIDDENdet1figure.jpg}}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_150degrees_onback_HIDDENendet1figure.jpg}}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_150degrees_onback_HIDDENdet2figure.jpg}}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_150degrees_onback_HIDDENendet2figure.jpg}}
\end{figure}


\begin{figure}
\caption{Event's captured by 160 degree separation. Detector 2's absolute location was 130 degrees. Detector 1's absolute location was 290 degrees.}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_160degrees_onback_HIDDENdet1figure.jpg}}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_160degrees_onback_HIDDENendet1figure.jpg}}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_160degrees_onback_HIDDENdet2figure.jpg}}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_160degrees_onback_HIDDENendet2figure.jpg}}
\end{figure}

\begin{figure}
\caption{Event's captured for 170 degree separation. Detector 2's absolute location was 130 degrees. Detector 1's absolute location was 300 degrees. }
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_170degrees_onback_HIDDENdet1figure.jpg}}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_170degrees_onback_HIDDENendet1figure.jpg}}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_170degrees_onback_HIDDENdet2figure.jpg}}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_170degrees_onback_HIDDENendet2figure.jpg}}
\end{figure}


\begin{figure}
\caption{Event's captured for 190 degree separation. Detector 2's absolute location was 130 degrees. Detector 1's absolute location was 320 degrees. }
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_190degrees_onback_HIDDENdet1figure.jpg}}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_190degrees_onback_HIDDENendet1figure.jpg}}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_190degrees_onback_HIDDENdet2figure.jpg}}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_190degrees_onback_HIDDENendet2figure.jpg}}
\end{figure}

\clearpage
\subsection{Theta 2 Figures}
These are the figures for the $ \Theta_2$ angle measurement. $\Theta_2$ had Detector 2 placed $ 90 \degree$ clockwise from it's initial position. On the lab bench, this position was noted as being $40 \degree$ for detector 2. 

\begin{figure}
\caption{Event's captured by 130 degree separation.  Detector 2's absolute location was 40 degrees. Detector 1's absolute location was 170 degrees.}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_130degrees_onback_HIDDEN_dist_search_right_way2det1figure.jpg}}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_130degrees_onback_HIDDEN_dist_search_right_way2endet1figure.jpg}}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_130degrees_onback_HIDDEN_dist_search_right_way2det2figure.jpg}}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_130degrees_onback_HIDDEN_dist_search_right_way2endet2figure.jpg}}
\end{figure}

\begin{figure}
\caption{Event's captured by 135 degree separation.  Detector 2's absolute location was 40 degrees. Detector 1's absolute location was 175 degrees.}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_135degrees_onback_HIDDEN_dist_search_right_way7det1figure.jpg}}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_135degrees_onback_HIDDEN_dist_search_right_way7endet1figure.jpg}}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_135degrees_onback_HIDDEN_dist_search_right_way7det2figure.jpg}}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_135degrees_onback_HIDDEN_dist_search_right_way7endet2figure.jpg}}
\end{figure}

\begin{figure}
\caption{Event's captured by 140 degree separation. Detector 2's absolute location was 40 degrees. Detector 1's absolute location was 180 degrees.}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_140degrees_onback_HIDDEN_dist_search_right_way6det1figure.jpg}}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_140degrees_onback_HIDDEN_dist_search_right_way6endet1figure.jpg}}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_140degrees_onback_HIDDEN_dist_search_right_way6det2figure.jpg}}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_140degrees_onback_HIDDEN_dist_search_right_way6endet2figure.jpg}}
\end{figure}


\begin{figure}
\caption{Event's captured by 145 degree separation. Detector 2's absolute location was 40 degrees. Detector 1's absolute location was 185 degrees.}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_145degrees_onback_HIDDEN_dist_search_right_way5det1figure.jpg}}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_145degrees_onback_HIDDEN_dist_search_right_way5endet1figure.jpg}}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_145degrees_onback_HIDDEN_dist_search_right_way5det2figure.jpg}}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_145degrees_onback_HIDDEN_dist_search_right_way5endet2figure.jpg}}
\end{figure}


\begin{figure}
\caption{Event's captured by 150 degree separation.  Detector 2's absolute location was 40 degrees. Detector 1's absolute location was 190 degrees.}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_150degrees_onback_HIDDEN_dist_search_right_way1det1figure.jpg}}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_150degrees_onback_HIDDEN_dist_search_right_way1endet1figure.jpg}}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_150degrees_onback_HIDDEN_dist_search_right_way1det2figure.jpg}}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_150degrees_onback_HIDDEN_dist_search_right_way1endet2figure.jpg}}
\end{figure}

\begin{figure}
\caption{Event's captured for 155 degree separation. Detector 2's absolute location was 40 degrees. Detector 1's absolute location was 195 degrees.}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_155degrees_onback_HIDDEN_dist_search_right_way4det1figure.jpg}}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_155degrees_onback_HIDDEN_dist_search_right_way4endet1figure.jpg}}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_155degrees_onback_HIDDEN_dist_search_right_way4det2figure.jpg}}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_155degrees_onback_HIDDEN_dist_search_right_way4endet2figure.jpg}}
\end{figure}

\begin{figure}
\caption{Event's captured for 160 degree separation. Detector 2's absolute location was 40 degrees. Detector 1's absolute location was 200 degrees.}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_160degrees_onback_HIDDEN_dist_search_right_way4det1figure.jpg}}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_160degrees_onback_HIDDEN_dist_search_right_way4endet1figure.jpg}}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_160degrees_onback_HIDDEN_dist_search_right_way4det2figure.jpg}}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_160degrees_onback_HIDDEN_dist_search_right_way4endet2figure.jpg}}

\end{figure}

\begin{figure}
\caption{Event's captured for 170 degree separation.  Detector 2's absolute location was 40 degrees. Detector 1's absolute location was 210 degrees.}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_170degrees_onback_HIDDEN_dist_search_right_way3det1figure.jpg}}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_170degrees_onback_HIDDEN_dist_search_right_way3endet1figure.jpg}}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_170degrees_onback_HIDDEN_dist_search_right_way3det2figure.jpg}}
\includegraphics[width = .5\textwidth]{\detokenize{Pics/20160223Na22Det1andDet2_170degrees_onback_HIDDEN_dist_search_right_way3endet2figure.jpg}}
\end{figure}



\end{document}
%
% ****** End of file apssamp.tex ******\begin{equation}

