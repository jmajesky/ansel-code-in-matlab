function [outputs] = ansel_lin_fit(means, variances, energies)
%cov_mat = variances * variances';
%inv_cov_mat = pinv(cov_mat);
%I wanted to be cool, but fine
%regressors = means' * inv_cov_mat * means * means' *inv_cov_mat * energies;

[outputs.fit_data, outputs.gof] = fit(means, energies, 'poly1', 'Weights', 1./variances);
outputs.fit_data
outputs.gof

end
