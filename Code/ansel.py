import numpy as np
import sys, os

def read_file(filename):
	datafile = open(filename)
	data = []
	for line in datafile.readlines():
		vals = line.split()

		data.append([vals[0], vals[1]])

	return np.array(data)


